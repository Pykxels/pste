#include "bibli.h"


using namespace std;

int main()
{

    vector <int> tab;
    int i;
    //Initialisation de la fonction aleatoire
    srand(time(NULL));

    for(i=0;i<20;i++) //Boucle pour parcourir le tableau
    {
        tab.push_back(-10+rand()%21); //Remplissage
    }

    sort (tab.begin(),tab.end());   //Tri du tableau

    for(i=0;i<20;i++)      //Boucle pour parcourir le tableau
    {
        cout << tab[i] <<endl;  //Affichage
    }

    return 0;
}
